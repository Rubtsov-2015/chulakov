import React, { Fragment, PureComponent } from 'react';
import axios from 'axios';
import _ from 'lodash';
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { Sort } from './components/sort';
import { ViewType } from "./components/sort";
import PersonsList from "./components/personsList";
import LanguagesList from './components/languagesList';
import { fetchUsers } from "./actions/usersActions";
import { lang } from "./actions/langActions";
import { isPreview } from "./actions/viewTypeActions";

class App extends PureComponent {
	state = {
		users: [],
	};

	componentDidMount() {
		this.props.isPreview(true);
		axios.get('/users')
			.then(users => {
				this.props.fetchUsers(users.data);
				this.setState({
					users: users.data,
				});
			})
			.catch(err => console.error(err));
	}

	triggerFavoriteUser = id => {
		const { fetchUsers, users } = this.props;
		const newUsers = Object.assign([], users);
		const user = _.find(newUsers, { 'id': id });
		user.favourite = !user.favourite;

		this.setState({users: newUsers}, () => fetchUsers(newUsers));
	};

	setSortableUsers = users => {
		this.setState({users: users});
	};

	setLanguage = lang => {
		this.props.lang(lang);
		this.setState({lang: lang});
	};

	render() {
		const { users } = this.props;
		const sortableUsers = this.state.users;

		return (
			<Fragment>
			<header className="main_header">
				<div className="center">
					<LanguagesList setLanguage={this.setLanguage} />
				</div>
			</header>

			<aside className="main_aside">
				<div className="center">
					<div className="sort_wrapper">
						<div className="sort_holder">
							<Sort
								setSortableData={this.setSortableUsers}
								sortableData={users}
								sortedData={sortableUsers}
							/>
						</div>
						<div className="sort_holder">
							<ViewType />
						</div>
					</div>
				</div>
			</aside>
			<main>
				<div className="center">
					<PersonsList users={sortableUsers} triggerFavoriteUser={this.triggerFavoriteUser }/>
				</div>
			</main>
		</Fragment>
		);
	}
}

App.propTypes = {
	users: PropTypes.array,
	lang: PropTypes.func,
	fetchUsers: PropTypes.func,
	isPreview: PropTypes.func
};

function mapStateToProps (state) {
	return {
		users: state.users,
		lang: state.lang,
	}
}

export default connect(mapStateToProps, { fetchUsers, lang, isPreview })(App);
