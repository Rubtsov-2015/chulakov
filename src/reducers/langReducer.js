const initialState = localStorage.getItem('language') ? localStorage.getItem('language') : 'english';

export default (state = initialState, action) => {
	switch (action.type) {
		case 'lang':
			localStorage.setItem('language', action.payload);
			return  action.payload;
		default:
			return state;
	}
}
