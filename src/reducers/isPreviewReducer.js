const initialState = true;

export default (state = initialState, action) => {
	switch (action.type) {
		case 'is_preview':
			return  action.payload;
		default:
			return state;
	}
}
