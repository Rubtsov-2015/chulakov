import { combineReducers } from 'redux';
import Users from './usersReducer';
import Lang from './langReducer';
import isPreview from './isPreviewReducer';

const allReducers = combineReducers({
	users: Users,
	lang: Lang,
	isPreview: isPreview,
});

export default allReducers;
