import React, { Fragment, PureComponent } from 'react';

import PropTypes from "prop-types";
import connect from "react-redux/es/connect/connect";
import t from "../../constans/translations";
import { isPreview } from "../../actions/viewTypeActions";

class ViewType extends PureComponent {
	render() {
		const { lang } = this.props;

		return (
			<Fragment>
				<div className="title_block">{t[lang].view}</div>
				<ul className="sort_list">
					<li onClick={() => this.props.isPreview(false)}><span>{t[lang].table}</span></li>
					<li onClick={() => this.props.isPreview(true)}><span>{t[lang].preview}</span></li>
				</ul>
			</Fragment>
		);
	}
}

ViewType.propTypes = {
	lang: PropTypes.string,
	isPreview: PropTypes.func
};

function mapStateToProps (state) {
	return {
		lang: state.lang,
	}
}

export default connect(mapStateToProps, { isPreview })(ViewType);
