import React, { Fragment, PureComponent } from 'react';
import _ from 'lodash';

import './style.scss';
import PropTypes from "prop-types";
import t from '../../constans/translations';
import connect from "react-redux/es/connect/connect";
import { fetchUsers } from "../../actions/usersActions";

class Sort extends PureComponent {
	state = {
		activeTypeSort: '',
		activeOrderSort: '',
	};

	sortByName = (data, searchString) => {
		window.scrollTo(window.scrollX, window.scrollY-1);
		window.scrollTo(window.scrollX, window.scrollY+1);
		const newData = Object.assign([], data);

		if (!searchString) {
			this.props.setSortableData(newData);
		}
		let regExp = new RegExp(`${searchString}`, 'gi'),
			result = -1,
			resultData = [];

		newData.forEach(item => {
			result = item.name.search(regExp);
			if (result > -1) {
				resultData.push(item);
			}
		});

		this.setState({
			activeTypeSort: 'name'
		});
		this.props.setSortableData(resultData);
	};

	sortByCount = (data, sortedField) => {
		window.scrollTo(window.scrollX, window.scrollY-1);
		window.scrollTo(window.scrollX, window.scrollY+1);
		if(this.state.activeTypeSort !== sortedField) {
			let newData = Object.assign([], data);
			newData = _.sortBy(newData, [sortedField]);

			this.props.setSortableData(newData);
			this.props.fetchUsers(newData);

			this.setState({
				activeTypeSort: sortedField,
				activeOrderSort: ''
			});
		}
	};

	setOrderSort = (typeOrder, data, sortedField) => {
		window.scrollTo(window.scrollX, window.scrollY-1);
		window.scrollTo(window.scrollX, window.scrollY+1);
		if(this.state.activeOrderSort !== typeOrder && this.state.activeTypeSort) {
			let newData = Object.assign([], data);
			newData = _.sortBy(newData, [sortedField]);
			if(typeOrder === 'descending'){
				this.props.fetchUsers(newData.reverse());
			} else {
				this.props.fetchUsers(newData);

			}
			this.setState({activeOrderSort: typeOrder});
			this.props.setSortableData(newData);
		}
	};

	render() {
		const { activeTypeSort, activeOrderSort } = this.state;
		const { sortableData, sortedData, lang } = this.props;

		return (
			<Fragment>
				<div className="title_block">{t[lang].sort}</div>
				<ul className="sort_list">
					<li className={activeTypeSort === 'id' ? 'active' : ''}>
						<span onClick={() => this.sortByCount(sortedData, 'id')}>ID</span>
					</li>
					<li className={activeTypeSort === 'name' ? 'active' : ''}>
						<span>
							<input type="text" placeholder={t[lang].name} onChange={val => this.sortByName(sortableData, val.target.value)} />
						</span>
					</li>
					<li className={activeTypeSort === 'age' ? 'active' : ''}>
						<span onClick={() => this.sortByCount(sortedData, 'age')}>{t[lang].age}</span>
					</li>
					<li className={activeOrderSort === 'ascending' ? 'active' : ''}>
						<span onClick={() => this.setOrderSort('ascending', sortedData, activeTypeSort)}>{t[lang].ascending}</span>
					</li>
					<li className={activeOrderSort === 'descending' ? 'active' : ''}>
						<span onClick={() => this.setOrderSort('descending', sortedData, activeTypeSort)}>{t[lang].descending}</span>
					</li>
				</ul>
			</Fragment>
		);
	}
}

Sort.propTypes = {
	sortableData: PropTypes.array,
	sortedData: PropTypes.array,
	fetchUsers: PropTypes.func,
	setSortableData: PropTypes.func,
	lang: PropTypes.string,
};

function mapStateToProps (state) {
	return {
		lang: state.lang,
	}
}

export default connect(mapStateToProps, { fetchUsers })(Sort);
