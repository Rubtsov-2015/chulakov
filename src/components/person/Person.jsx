import React, { PureComponent } from 'react';

import './style.scss';
import PropTypes from "prop-types";
import t from '../../constans/translations';
import connect from "react-redux/es/connect/connect";

class Person extends PureComponent {
	state = {
		user: {},
		preview: true,
		animateStar: false,
	};

	static getDerivedStateFromProps(nextProps, prevState){
		if(nextProps !== prevState){
			return {
				user: nextProps.user,
				preview: nextProps.isPreview,
			};
		}
		else return null;
	}

	playVideo = element => {
		if(this.props.isVisible && element !== null){
			element.play();
		} else if(!this.props.isVisible && element !== null){
			element.pause();
		}
	};

	animateStar = () => {
		this.setState({animateStar: true}, () => {
			setTimeout(() => {
				this.setState({animateStar: false})
			}, 400)
		})
	};


	render() {
		const { lang, triggerFavoriteUser, isVisible } = this.props;
		const { user, preview, animateStar } = this.state;
		const {
			image,
			favourite,
			name,
			phone,
			age,
			id,
			video,
			phrase
		} = user;
		const imagePath = `/images/${image}.svg`;

		return (
			<div className={` person_item ${preview && video ? 'video' : ''} ${preview ? 'preview' : ''} ${isVisible ? 'visible' : ''}`}>
				<div className="person_item_content">
					<div className="person_short_info">
						<div className="photo_holder">
							<img src={imagePath} alt={`${image}`} />
						</div>
						<div className="name">{name}</div>
					</div>
					<div className="years">{age} {t[lang].years}</div>
					<div className="number">
						<a href="http://google.com">{phone}</a>
					</div>
					{preview && phrase && <div className="phrase">{phrase}</div>}
					<div className={`favourite ${favourite ? 'isFavourite': ''}`} >
						<span
							onClick={() => { triggerFavoriteUser(id); this.animateStar();}}
							className={`${animateStar ? 'active' : ''}`}
						>
							<img src="./images/star.svg" alt="favourite"/>
							<img src="./images/star.svg" alt="" className="hidden_image"/>
						</span>
					</div>
				</div>
				{preview && video && <div className="video_holder">
					<video controls ref={e => this.playVideo(e)}><source src={`./videos/${video}.mp4`} /></video>
				</div>}
			</div>
		);
	}
}

Person.propTypes = {
	user: PropTypes.object.isRequired,
	triggerFavoriteUser: PropTypes.func,
	lang: PropTypes.string,
	isPreview: PropTypes.bool,
	isVisible: PropTypes.any
};

function mapStateToProps (state) {
	return {
		lang: state.lang,
		isPreview: state.isPreview
	}
}

export default connect(mapStateToProps)(Person);
