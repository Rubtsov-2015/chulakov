import React from "react";
import PropTypes from "prop-types";
import './style.scss';

const LanguagesList = props => (
	<ul className="language_list">
		<li onClick={() => props.setLanguage('english')}>Eng</li>
		<li onClick={() => props.setLanguage('russian')}>Rus</li>
	</ul>
);

LanguagesList.propTypes = {
	setLanguage: PropTypes.func
};

export default LanguagesList;
