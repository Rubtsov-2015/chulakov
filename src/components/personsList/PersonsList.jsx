import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import TrackVisibility from 'react-on-screen';

import './style.scss';
import Person from '../person';
import connect from "react-redux/es/connect/connect";
import { fetchUsers } from "../../actions/usersActions";

class PersonsList extends PureComponent {
	render() {
		const { users, triggerFavoriteUser } = this.props;

		return (
			<div className="persons_list">
				{users && users.map(user => (
					<TrackVisibility key={user.id} partialVisibility={true}>
						<Person user={{...user}} triggerFavoriteUser={triggerFavoriteUser} />
					</TrackVisibility>
				))}
			</div>
		);
	}
}

PersonsList.propTypes = {
	users: PropTypes.array,
	fetchUsers: PropTypes.func,
	triggerFavoriteUser: PropTypes.func
};

function mapStateToProps (state) {
	return {
		lang: state.lang,
		fetchUsers: state.fetchUsers,
	}
}

export default connect(mapStateToProps, { fetchUsers })(PersonsList);
