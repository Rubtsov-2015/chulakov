export const ISPREVIEW = 'is_preview';

export function isPreview(isPreview) {
	return {
		type: ISPREVIEW,
		payload: isPreview,
	};
}
