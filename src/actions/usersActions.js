export const FETCH_USERS = 'fetch_users';

export function fetchUsers(users) {
	return {
		type: FETCH_USERS,
		payload: users,
	};
}
