export const LANG = 'lang';

export function lang(lang) {
	return {
		type: LANG,
		payload: lang,
	};
}
