export default {
	english: {
		sort: 'Sort by',
		years: 'years',
		age: 'Age',
		ascending: 'Ascending',
		descending: 'Descending',
		view: 'Views',
		preview: 'Preview',
		table: 'Table',
		name: 'Name'
	},
	russian: {
		sort: 'Сортировка',
		years: 'лет',
		age: 'Возраст',
		ascending: 'По возрастанию',
		descending: 'По убыванию',
		view: 'Вид',
		preview: 'Превью',
		table: 'Таблица',
		name: 'Имя'
	},
};
